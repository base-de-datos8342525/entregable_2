# 1
```
SELECT COD_DIS, TIP_VEN, TIP_CLI, COUNT(*) AS CANTIDAD_PERSONAS
FROM (SELECT COD_DIS, TIP_VEN, NULL AS TIP_CLI FROM VENDEDOR
      UNION ALL
      SELECT COD_DIS, NULL AS TIP_VEN, TIP_CLI FROM CLIENTE) AS PERSONAS
GROUP BY CUBE (COD_DIS, TIP_VEN, TIP_CLI);

```
# 2
```
SELECT COD_DIS, TIP_CLI, COUNT(*) AS CANTIDAD_CLIENTES
FROM CLIENTE
GROUP BY ROLLUP (COD_DIS), TIP_CLI;

```
# 3
```
SELECT COUNT(*) AS TOTAL_PRODUCTOS
FROM PRODUCTO;

``` 
# 4
```
SELECT COD_DIS, COUNT(*) AS CANTIDAD_VENDEDORES
FROM VENDEDOR
GROUP BY COD_DIS;

```

# 5
```
SELECT COD_DIS, TIP_VEN, TIP_CLI, COUNT(*) AS CANTIDAD_PERSONAS
FROM (SELECT COD_DIS, TIP_VEN, NULL AS TIP_CLI FROM VENDEDOR
      UNION ALL
      SELECT COD_DIS, NULL AS TIP_VEN, TIP_CLI FROM CLIENTE) AS PERSONAS
GROUP BY GROUPING SETS ((COD_DIS), (COD_DIS, TIP_VEN), (COD_DIS, TIP_CLI));


```